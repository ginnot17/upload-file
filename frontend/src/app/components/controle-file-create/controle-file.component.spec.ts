import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControleFileComponent } from './controle-file.component';

describe('ControleFileComponent', () => {
  let component: ControleFileComponent;
  let fixture: ComponentFixture<ControleFileComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ControleFileComponent]
    });
    fixture = TestBed.createComponent(ControleFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
