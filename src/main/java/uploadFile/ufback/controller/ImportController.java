package uploadFile.ufback.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uploadFile.ufback.dtos.FilePathRequest;
import uploadFile.ufback.services.ImportService;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/import")
public class ImportController {


    private final ImportService importService;
    @PostMapping("/csv/headers")
    public ResponseEntity<List<String>> getCSVHeaders(@RequestBody FilePathRequest request) {
        String filePath = request.getFilePath();
        try {
            List<String> csvHeaders = importService.getCSVHeadersFromCSV(filePath);
            return ResponseEntity.ok(csvHeaders);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Collections.emptyList());
        }
    }
}
