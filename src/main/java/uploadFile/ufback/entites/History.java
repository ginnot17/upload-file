package uploadFile.ufback.entites;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String file_name;
    private Date date_create;
    private double volume;

    @ManyToOne
    @JoinColumn(name = "fk_user_upload")
    private User userUpload;

    @ManyToOne
    @JoinColumn(name = "fk_parameters")
    private Parameters parameters;

}

