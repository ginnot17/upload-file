import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AxiosService } from 'src/app/services/axios.service';

interface errors {
  [key: string]: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  loading: boolean = false;
  firstName: string = '';
  lastName: string = '';
  login: string = '';
  password: string = '';
  passwordConfirm: string = '';
  errors: errors = {
    firstName: '',
    lastName: '',
    login: '',
    password: '',
    passwordConfirm: '',
  };

  constructor(private axiosService: AxiosService, private router: Router) {}

  onSubmitRegister(): void {
    this.loading = true;
    this.errors['firstName'] = '';
    this.errors['lastName'] = '';
    this.errors['login'] = '';
    this.errors['password'] = '';
    this.errors['passwordConfirm'] = '';
    if (this.passwordConfirm !== this.password) {
      this.errors['passwordConfirm'] =
        'Password and Confirm Password must be compliant';
      this.loading = false;
      return;
    }
    this.axiosService
      .request('POST', '/register', {
        firstName: this.firstName,
        lastName: this.lastName,
        login: this.login,
        password: this.password,
      })
      .then((response) => {
        this.axiosService.setAuthToken(response.data.token);
        this.router.navigate(['/upload']);
      })
      .catch((error) => {
        console.log(error);

        if (error.response.data && Array.isArray(error.response.data)) {
          const errorData: { field: string; message: string }[] =
            error.response.data;
          console.log(error.response.data);
          for (const errorItem of errorData) {
            if (errorItem.field in this.errors) {
              this.errors[errorItem.field] = errorItem.message;
            }
          }

          console.log(this.errors);
        }

        this.axiosService.setAuthToken(null);
      });

    this.loading = false;
  }
}
