package uploadFile.ufback.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import uploadFile.ufback.entites.History;

public interface HistoryRepository extends JpaRepository<History, Long> {
}
