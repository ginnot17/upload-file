import { Component,Input } from '@angular/core';
import {
  CdkDragDrop,
  CdkDropList,
  CdkDrag,
  moveItemInArray,
} from '@angular/cdk/drag-drop';
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-controle-file',
  templateUrl: './controle-file.component.html',
  styleUrls: ['./controle-file.component.css'],
  standalone: true,
  imports: [CdkDropList, NgFor, CdkDrag],
})
export class ControleFileComponent {
  @Input() csvHeaders: string[] = [];

  drop1(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.csvHeaders, event.previousIndex, event.currentIndex);
    console.log(this.csvHeaders);
    
  }
  
}
