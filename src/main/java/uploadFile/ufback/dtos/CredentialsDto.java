package uploadFile.ufback.dtos;

public record CredentialsDto (String login, char[] password) { }
