package uploadFile.ufback.mappers;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import uploadFile.ufback.dtos.SignUpDto;
import uploadFile.ufback.dtos.UserDto;
import uploadFile.ufback.entites.User;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto toUserDto(User user);

    @Mapping(target = "password", ignore = true)
    User signUpToUser(SignUpDto signUpDto);

}