package uploadFile.ufback.config;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import uploadFile.ufback.dtos.ErrorDto;
import uploadFile.ufback.exception.AppExeption;

import java.util.*;

@ControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler(value = {AppExeption.class})
    @ResponseBody
    public ResponseEntity<List<Map<String, String>>> handleException(AppExeption ex) {
        List<Map<String, String>> errorDetailsList = new ArrayList<>();
        Map<String, String> errorDetails = new HashMap<>();
        errorDetails.put("field",ex.getField());
        errorDetails.put("message",ex.getMessage());
        errorDetailsList.add(errorDetails);
        return ResponseEntity.status(ex.getStatus())
                .body(errorDetailsList);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();

        List<Map<String, String>> validationErrors = new ArrayList<>();

        for (ConstraintViolation<?> violation : violations) {
            Map<String, String> errorDetails = new HashMap<>();
            errorDetails.put("field", violation.getPropertyPath().toString());
            errorDetails.put("message", violation.getMessage());
            validationErrors.add(errorDetails);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(validationErrors);
    }


}
