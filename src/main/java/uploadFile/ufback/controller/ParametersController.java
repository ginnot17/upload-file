package uploadFile.ufback.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uploadFile.ufback.dtos.ParametersDto;
import uploadFile.ufback.services.ParametersService;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/parameters")
public class ParametersController {

    private final ParametersService parametersService;

    @GetMapping("/userIdAcces/{userId}")
    public ResponseEntity<List<ParametersDto>> getParametersByUserAcces(@PathVariable Long userId) {
        return ResponseEntity.ok(parametersService.ListParametersByUserAccess(userId));
    }


}
