package uploadFile.ufback.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParametersDto {
    private Long id;
    private String name;
    private String field;
    private String list_order;
}
