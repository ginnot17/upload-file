import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpRequest,
  HttpEvent,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AxiosService } from './axios.service';

@Injectable({
  providedIn: 'root',
})
export class FileUploadService {
  private baseUrl = 'http://localhost:8080';

  constructor(private http: HttpClient, private axiosService: AxiosService) {}

  upload(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('files', file);
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.axiosService.getAuthToken()}`,
    });

    const req = new HttpRequest(
      'POST',
      `${this.baseUrl}/file/upload`,
      formData,
      {
        headers: headers,
        reportProgress: true,
        responseType: 'json',
      }
    );

    return this.http.request(req);
  }

  getUserDirectory() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.axiosService.getAuthToken()}`,
    });

    return this.http.get<{ filePath: string }>(`${this.baseUrl}/file/user-directory`, {
      headers: headers,
    });
  }

  // getFiles(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/files`);
  // }
}
