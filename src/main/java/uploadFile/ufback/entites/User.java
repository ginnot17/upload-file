package uploadFile.ufback.entites;


import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.UniqueElements;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "app_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", nullable = false)
    @NotBlank(message = "First Name could not be blank .")
    @Size(max = 100)
    private String firstName;

    @NotBlank(message = "Last Name could not be blank .")
    @Column(name = "last_name", nullable = false)
    @Size(max = 100)
    private String lastName;

    @Column(nullable = false)
    @NotBlank(message = "Login could not be blank.")
    @Size(max = 100)
    @Email(message="This field must be an email adress valid")
    private String login;

    @Column(nullable = false)
    @NotBlank(message = "Password could not be blank.")
    @Size(min = 8,message = "Password must contain at least 8 characters")
    private String password;

    @ManyToMany
    @JoinTable(
            name = "acces",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "parameters_id")
    )
    private Set<Parameters> parameters = new HashSet<>();
}
