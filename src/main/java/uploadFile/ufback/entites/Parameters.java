package uploadFile.ufback.entites;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table
public class Parameters {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(columnDefinition = "text")
    private String field;

    @Column(columnDefinition = "text")
    private String list_order;

    @ManyToOne
    @JoinColumn(name = "fk_user_create")
    private User userCreate;

    @ManyToMany(mappedBy = "parameters")
    private Set<User> users = new HashSet<>();


}
