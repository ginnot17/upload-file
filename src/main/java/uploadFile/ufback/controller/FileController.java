package uploadFile.ufback.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uploadFile.ufback.exception.AppExeption;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/file")
public class FileController {

    // define location
    public static final String DIRECTORY = System.getProperty("user.home") + "/Downloads/uploads";

    @PostMapping("/upload")
    public ResponseEntity<Map<String, List<String>>> uploadFiles(@RequestParam("files") List<MultipartFile> multipartFiles) {
        List<String> filenames = new ArrayList<>();
        for (MultipartFile file : multipartFiles) {
            String filename = StringUtils.cleanPath(file.getOriginalFilename());
            if (!isCSVFile(file)) {
                throw new AppExeption("The file " + filename + " is not CSV type", HttpStatus.BAD_REQUEST,"");
            }
            Path fileStorage = Path.of(DIRECTORY, filename).toAbsolutePath().normalize();
            try {
                Files.copy(file.getInputStream(), fileStorage, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
            }
            filenames.add(filename);
        }

        Map<String, List<String>> response = new HashMap<>();
        response.put("success", filenames);

        return ResponseEntity.ok().body(response);
    }

    private boolean isCSVFile(MultipartFile file) {
        String contentType = file.getContentType();
        return contentType != null && contentType.equals("text/csv");
    }

    @GetMapping("/user-directory")
    public ResponseEntity<Map<String, String>> getUserDirectory() {
        String userDirectory = System.getProperty("user.home") + "\\Downloads\\uploads";
        Map<String, String> response = new HashMap<>();
        response.put("filePath", userDirectory);

        return ResponseEntity.ok(response);
    }
}
