package uploadFile.ufback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan("uploadFile.ufback.entites")
public class UfBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(UfBackApplication.class, args);
	}

}
