package uploadFile.ufback.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uploadFile.ufback.entites.Parameters;

import java.util.List;
import java.util.Optional;

public interface ParametersRepository extends JpaRepository<Parameters, Long> {


    // get parameters by user_id in the acces table
    @Query(value = "SELECT p.* FROM parameters p " +
            "JOIN acces a ON a.parameters_id = p.id " +
            "WHERE a.user_id = :userId", nativeQuery = true)
    List<Parameters> findParametersByUserIdInAccesTable(@Param("userId") Long user_id);

}
