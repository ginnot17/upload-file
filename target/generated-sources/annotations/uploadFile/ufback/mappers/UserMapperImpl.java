package uploadFile.ufback.mappers;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import uploadFile.ufback.dtos.SignUpDto;
import uploadFile.ufback.dtos.UserDto;
import uploadFile.ufback.entites.User;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-11-07T07:03:24+0300",
    comments = "version: 1.5.0.Final, compiler: javac, environment: Java 21.0.1 (Oracle Corporation)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public UserDto toUserDto(User user) {
        if ( user == null ) {
            return null;
        }

        UserDto.UserDtoBuilder userDto = UserDto.builder();

        userDto.id( user.getId() );
        userDto.firstName( user.getFirstName() );
        userDto.lastName( user.getLastName() );
        userDto.login( user.getLogin() );

        return userDto.build();
    }

    @Override
    public User signUpToUser(SignUpDto signUpDto) {
        if ( signUpDto == null ) {
            return null;
        }

        User.UserBuilder user = User.builder();

        user.firstName( signUpDto.firstName() );
        user.lastName( signUpDto.lastName() );
        user.login( signUpDto.login() );

        return user.build();
    }
}
