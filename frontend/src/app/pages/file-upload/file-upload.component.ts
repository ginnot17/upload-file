import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { AxiosService } from 'src/app/services/axios.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
})
export class FileUploadComponent implements OnInit {
  selectedFiles?: FileList;
  currentFile?: File;
  progress = 0;
  message = '';
  options: any[] = [];
  valideFile: boolean = false;
  SelectedOption: string = '';
  csvHeaders: string[] = [];
  filePath: string = '';

  fileInfos?: Observable<any>;

  constructor(
    private uploadService: FileUploadService,
    private axiosService: AxiosService,
    private http: HttpClient
  ) {}

  ngOnInit(): void {}

  selectFile(event: any): void {
    this.selectedFiles = event.target.files;
  }

  upload(): void {
    this.progress = 0;
    this.options=[]
    this.valideFile=false
    this.SelectedOption=''
    if (this.selectedFiles) {
      const file: File | null = this.selectedFiles.item(0);

      if (file) {
        this.currentFile = file;

        this.uploadService.upload(this.currentFile).subscribe({
          next: (event: any) => {
            this.valideFile = false;
            if (event.type === HttpEventType.UploadProgress) {
              this.progress = Math.round((100 * event.loaded) / event.total);
              // this.message="file upload successful"
            } else if (event instanceof HttpResponse) {
              if (event.body.message) {
                this.message = event.body.message;
              } else {
                this.message = event.body.success;

                // list option
                this.axiosService
                  .request(
                    'GET',
                    '/parameters/userIdAcces/' + this.axiosService.getUserId(),
                    {}
                  )
                  .then((response) => {
                    console.log(response.data);

                    this.options = response.data;
                  })
                  .catch((error) => {
                    console.log(error.response);
                  });
              }
            }
            this.valideFile = true;
            // console.log(event.body);
          },
          error: (err: any) => {
            this.valideFile = false;
            // console.log(err);
            this.progress = 0;
            console.log(err.error);

            if (Array.isArray(err.error)) {
              this.message = err.error.map(
                (val: { message: string }) => val.message
              );
            } else {
              this.message = err.error.message;
            }

            this.currentFile = undefined;
          },
        });
      }

      this.selectedFiles = undefined;
    }
  }

  fetchCSVHeaders() {
    if (this.SelectedOption === '0') {
      this.uploadService.getUserDirectory().subscribe((response) => {
        this.filePath = response.filePath + '\\' + this.currentFile?.name;
        this.axiosService
          .request('POST', '/import/csv/headers', { filePath: this.filePath })
          .then((response) => {
            this.csvHeaders = response.data;
            console.log(this.csvHeaders);
          })
          .catch();
      });
    }
  }
}
