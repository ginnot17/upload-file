package uploadFile.ufback.services;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uploadFile.ufback.dtos.ParametersDto;
import uploadFile.ufback.entites.Parameters;
import uploadFile.ufback.exception.AppExeption;
import uploadFile.ufback.mapper.ParametersMapper;
import uploadFile.ufback.repositories.ParametersRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ParametersService {
    private final ParametersRepository parametersRepository;
    private final ParametersMapper parametersMapper;

    public List<ParametersDto> ListParametersByUserAccess(Long userId) {
        List<Parameters> parametersList = parametersRepository.findParametersByUserIdInAccesTable(userId);
        if (parametersList.isEmpty()) {
            throw new AppExeption("User not found in the Acces list", HttpStatus.NOT_FOUND, "");
        } else {
            return parametersMapper.ListParametersToListParametersDto(parametersList);
        }
    }
}
