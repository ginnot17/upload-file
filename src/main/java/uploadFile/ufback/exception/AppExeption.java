package uploadFile.ufback.exception;

import org.springframework.http.HttpStatus;

public class AppExeption extends RuntimeException {
    private final HttpStatus status;
    private final String field;

    public AppExeption(String message, HttpStatus status, String field) {
        super(message);
        this.status = status;
        this.field = field;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getField() {
        return field;
    }
}
