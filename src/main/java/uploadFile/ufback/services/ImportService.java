package uploadFile.ufback.services;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ImportService {
    public List<String> getCSVHeadersFromCSV(String filePath) throws IOException {
        try (Reader reader = new FileReader(filePath);
             CSVReader csvReader = new CSVReader(reader)) {
            String[] headers;
            try {
                headers = csvReader.readNext();
            } catch (CsvValidationException ex) {
                ex.printStackTrace();
                headers = null;
            }

            if (headers != null) {
                return Arrays.asList(headers);
            } else {
                return Collections.emptyList();
            }
        }
    }


}
