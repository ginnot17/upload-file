package uploadFile.ufback.mapper;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import uploadFile.ufback.dtos.ParametersDto;
import uploadFile.ufback.entites.Parameters;

import java.util.ArrayList;
import java.util.List;

@Component
public class ParametersMapper {


    public static List<ParametersDto> ListParametersToListParametersDto(List<Parameters> parameters){
        List<ParametersDto> parametersDtoList = new ArrayList<>();

        for (Parameters parameter : parameters ){
            ParametersDto dto = new ParametersDto();
            dto.setId(parameter.getId());
            dto.setName(parameter.getName());
            dto.setField(parameter.getField());
            dto.setList_order((parameter.getList_order()));
            parametersDtoList.add(dto);
        }
        return parametersDtoList;

    }
}
