import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AxiosService } from 'src/app/services/axios.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  constructor(private router: Router, private axiosService: AxiosService) {}
  isActive(route: string): boolean {
    return this.router.url === route;
  }
  logout() {
    this.axiosService.setAuthToken(null);
    this.router.navigate(['/login']);
  }
}
