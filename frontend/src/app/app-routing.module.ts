import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileUploadComponent } from './pages/file-upload/file-upload.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { AuthGuard, LoginGuard } from './services/auth-guard.service';
import { AppComponent } from './app.component';

const routes: Routes = [
  { path: 'upload', component: FileUploadComponent,canActivate:[AuthGuard] },
  { path: 'login', component: LoginComponent ,canActivate:[LoginGuard]},
  { path: 'register', component: RegisterComponent,canActivate:[LoginGuard] },
  { path: '', component: AppComponent,canActivate:[AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
