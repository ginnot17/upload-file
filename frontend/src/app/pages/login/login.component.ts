import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AxiosService } from 'src/app/services/axios.service';

interface errors {
  [key: string]: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  loading: boolean = false;
  login: string = '';
  password: string = '';
  errors: errors = {
    login: '',
    password: '',
  };

  constructor(private axiosService: AxiosService, private router: Router) {}

  onSubmitLogin(): void {
    this.errors['login'] = '';
    this.errors['password'] = '';

    this.axiosService
      .request('POST', '/login', {
        login: this.login,
        password: this.password,
      })
      .then((response) => {
        this.axiosService.setAuthToken(response.data.token);
        this.router.navigate(['/upload']);
      })
      .catch((error) => {
        console.log(error);

        if (error.response.data && Array.isArray(error.response.data)) {
          const errorData: { field: string; message: string }[] =
            error.response.data;
          console.log(error.response.data);
          for (const errorItem of errorData) {
            if (errorItem.field in this.errors) {
              this.errors[errorItem.field] = errorItem.message;
            }
          }

          console.log(this.errors);
        }

        this.axiosService.setAuthToken(null);
      });

    this.loading = false;
  }
}
