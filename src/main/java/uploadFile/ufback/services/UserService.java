package uploadFile.ufback.services;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uploadFile.ufback.dtos.CredentialsDto;
import uploadFile.ufback.dtos.SignUpDto;
import uploadFile.ufback.dtos.UserDto;
import uploadFile.ufback.entites.User;
import uploadFile.ufback.mappers.UserMapper;
import uploadFile.ufback.repositories.UserRepository;
import uploadFile.ufback.exception.AppExeption;

import java.nio.CharBuffer;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final UserMapper userMapper;

    public UserDto login(CredentialsDto credentialsDto) {
        User user = userRepository.findByLogin(credentialsDto.login())
                .orElseThrow(() -> new AppExeption("Unknown user", HttpStatus.NOT_FOUND, "login"));

        if (passwordEncoder.matches(CharBuffer.wrap(credentialsDto.password()), user.getPassword())) {
            return userMapper.toUserDto(user);
        }
        throw new AppExeption("Invalid password", HttpStatus.BAD_REQUEST, "password");
    }

    public UserDto register(SignUpDto userDto) {
        Optional<User> optionalUser = userRepository.findByLogin(userDto.login());

        if (optionalUser.isPresent()) {
            throw new AppExeption("Login already exists", HttpStatus.BAD_REQUEST, "login");
        }

        User user = userMapper.signUpToUser(userDto);
        if (userDto.password() == null || userDto.password().length == 0) {
            user.setPassword("");
        } else if (userDto.password().length > 0 && userDto.password().length < 8) {
            //throw new AppExeption("Password must contain at least 8 characters", HttpStatus.BAD_REQUEST, "password");
            user.setPassword("1");
        } else {
            user.setPassword(passwordEncoder.encode(CharBuffer.wrap(userDto.password())));
        }

        User savedUser = userRepository.save(user);

        return userMapper.toUserDto(savedUser);
    }

    public UserDto findByLogin(String login) {
        User user = userRepository.findByLogin(login)
                .orElseThrow(() -> new AppExeption("Unknown user", HttpStatus.NOT_FOUND, ""));
        return userMapper.toUserDto(user);
    }

}