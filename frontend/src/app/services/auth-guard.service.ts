import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AxiosService } from './axios.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private axiosService: AxiosService, private router: Router) {}

  canActivate(): boolean {
    if (this.axiosService.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
@Injectable()
export class LoginGuard implements CanActivate {
  constructor(private axiosService: AxiosService, private router: Router) {}

  canActivate(): boolean {
    if (this.axiosService.isAuthenticated()) {
      this.router.navigate(['/upload']);
      return false;
    } else {
      return true;
    }
  }
}
